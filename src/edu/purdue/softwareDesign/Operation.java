package edu.purdue.softwareDesign;

public abstract class Operation {
	
	private Operation opr;
	private int number1;
	private int number2;
	
	public int getNumber1() {
		return number1;
	}
	public void setNumber1(int number1) {
		this.number1 = number1;
	}
	public int getNumber2() {
		return number2;
	}
	public void setNumber2(int number2) {
		this.number2 = number2;
	}
	public Operation()
	{
		
	}
	public Operation(int number1, int number2){
		setNumber1(number1);
		setNumber2(number2);
	}
	public Operation(int operation)
	{
		switch(operation){
		case 1: opr = new Addition();
		break;
		}
	}
	public abstract void Calculate();
}
