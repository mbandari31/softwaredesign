package edu.purdue.softwareDesign;

public class Division extends Operation{

	public Division(int number1, int number2){
		super.setNumber1(number1);
		super.setNumber2(number2);
	}
	@Override
	public void Calculate()
	{
		System.out.println(super.getNumber1() / super.getNumber2());
	}
}
